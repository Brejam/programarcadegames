__author__ = 'Ryan'
print('''
7.1 Data Types
So far this book has shown four types of data:

    String
    Integer
    Floating point
    Boolean

Python can display what type of data a value is with the type function.

This type function isn't useful for other programming in this book, but it is good to demonstrate the types of data introduced so far.
Type the following into the interactive IDLE shell.
(Don't create a new window and type this in as a program; it won't work.)

print(type(3))
print(type(3.145))
print(type("Hi there"))
print(type(True))

Output:
<class 'int'>
<class 'float'>
<class 'str'>
<class 'bool'>

It is also possible to use the type function on a variable to see what kind of data is in it.
x = 3
type(x)

More than one coin to collect? Use a list!
The two new types of data introduced in this chapter are Lists and Tuples.
Try running the following commands in the interactive Python shell and see what is displayed:
print(type( (2, 3, 4, 5) ))
print(type( [2, 3, 4, 5] ))
-----
''')

print(type(3))
print(type(3.145))
print(type("Hi there"))
print(type(True))
print()
print(type((2, 3, 4, 5)))
print(type([2, 3, 4, 5]))

print()
print('''
7.2 Working With Lists
To create a list and print it out, try the following:

x = [1,2]
print(x)
[1, 2]

To print an individual element in an array:
print(x[0])
1

This number with the item's location is called the index.
List locations start at zero. A list with 10 elements does not have an element in spot [10].
Just spots [0] through [9].

A program can assign new values to an individual element in a list.
In the case below, the first spot at location zero (not one) is assigned the number 22:

x[0] = 22
print(x)
[22, 2]
-----
''')

x = [1, 2]
print(x)
print(x[0])
print()
x[0] = 22
print(x)

print()
print('''
7.3 Iterating Through a List
There are two types of for loops to iterate through lists

The format of the command:
for item_variable in list_name:

my_list = [101, 20, 10]
for item in my_list:
    print(item)

101
20
10

my_list = [ [2,3], [4,3], [6,7] ]
for item in my_list:
    print(item)

[2,3]
[4,3]
[6,7]

The second format is to use an index variable and the range function.
Get the length of the list using the len function:

my_list = ["Pie", "Peanut butter", "Bread"]
for item in range(len(my_list)):
    print(my_list[item])

Pie
Peanut butter
Bread

This method is more complex, but is also more powerful.
Because we are working directly with the list elements, rather than a copy, the list can be modified.
The for-each loop does not allow modification of the original list.
-----
''')

my_list = [101, 20, 10]
for item in my_list:
    print(item)

print()

my_list = [ [2,3], [4,3], [6,7] ]
for item in my_list:
    print(item)

print()

my_list = ["Pie", "Peanut butter", "Bread"]
for item in range(len(my_list)):
    print(my_list[item])

print()
print('''
7.4 Adding to a List
New items can be added to lists (not tuples) using the append command.

my_list = [2, 4]
print(my_list)
my_list.append(9)
print(my_list)

[2, 4]
[2, 4, 9]

Creating a list of numbers from user input

my_list = [] # Empty list
for i in range(3):
    userInput = input("Enter an integer: ")
    userInput = int(userInput)
    my_list.append(userInput)
    print(my_list)

e.g.
Enter an integer: 4
[4]
Enter an integer: 5
[4, 5]
Enter an integer: 3
[4, 5, 3]

# Create an array with 100 zeros.
my_list = [0] * 100
-----
''')

my_list = [2, 4]
print(my_list)
my_list.append(9)
print(my_list)

print()

my_list = []  # Empty list
for i in range(3):
    #  userInput = input("Enter an integer: ")
    userInput = 4
    my_list.append(userInput)
    print(my_list)

print()
print('''
7.5 Summing or Modifying a List
Creating a running total of an array is a common operation. Here's how it is done:

Summing the values in a list v1:
# Copy of the array to sum
myArray = [5, 76, 8, 5, 3, 3, 56, 5, 23]

# Initial sum should be zero
arrayTotal = 0

# Loop from 0 up to the number of elements
# in the array:
for i in range(len(myArray)):
    # Add element 0, next 1, then 2, etc.
    arrayTotal += myArray[i]

# Print the result
print(arrayTotal)

184

Summing the values in a list v2
myArray = [5, 76, 8, 5, 3, 3, 56, 5, 23]
arrayTotal = 0

for item in myArray:
    arrayTotal += item
print(arrayTotal)

184

However version 2 could not double the values in an array.
Why? Because item is a copy of an element in the array.
It would double the copy, not the original array element.

7.6 Slicing Strings
Strings are actually lists of characters.
They can be treated like lists with each letter a separate item.

-----
''')

myArray = [5, 76, 8, 5, 3, 3, 56, 5, 23]
arrayTotal = 0
for i in range(len(myArray)):
    arrayTotal += myArray[i]
print(arrayTotal)

print()

myArray = [5, 76, 8, 5, 3, 3, 56, 5, 23]
arrayTotal = 0
for item in myArray:
    arrayTotal += item
print(arrayTotal)

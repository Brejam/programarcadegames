__author__ = 'Ryan'
# Create a Quiz

print("Short Quiz")
print("----------")
print()

correctAnswers = 0
numberOfQs = 5

print("Question 1:")
if input("What is my name? ").lower() == "ryan":
    print("Correct!")
    correctAnswers += 1
else:
    print("Oops, incorrect.")
print()

print("Question 2:")
if int(input("What is 7 * 7? ")) == 49:
    print("Correct!")
    correctAnswers += 1
else:
    print("Sorry, wrong.")
print()

print("Question 3:")
print("What is the name of the Queen?")
print("1. Joan")
print("2. Anne")
print("3. Elizabeth")
if int(input("? ")) == 3:
    print("Correct!")
    correctAnswers += 1
else:
    print("No")
print()

print("Question 4:")
if input("What is the last letter of the alphabet? ").lower() == "z":
    print("Correct!")
    correctAnswers += 1
else:
    print("Bad answer.")
print()

print("Question 5:")
if int(input("How many 'd's in 'Edward Woodward'? ")) == 4:
    print("That's right!")
    correctAnswers += 1
else:
    print("Boo boo.")
print()

print("Results")
print("-------")
if correctAnswers > 3:
    print("Congratulations! You got", correctAnswers, "answers right!")
else:
    print("You got", correctAnswers, "answers right.")
percentCorrect = correctAnswers / numberOfQs * 100

print("That is a score of", percentCorrect, "percent.")


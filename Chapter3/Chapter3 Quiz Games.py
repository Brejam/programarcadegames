__author__ = 'Ryan'
# 3.1 Basic Comparisons
a = 4
b = 5
c = 5

if a < b:
    print("a is less than b")

if a >= b:
    print("a is greater than or equal to b")

if a == c:
    print("a is equal to c")
if a != c:
    print("a is not equal to c")
print("Done")
print()

# 3.2 Indentation
if a == 4:
    print("If a is 4, this will print")
    print("So will this")
print("This always prints")
print()

# 3.3 Using And/Or
# And
if a < b and a < c:
    print("a is less than b and c")

# Or
if a < b or a < c:
    print("a is less than either b or c (or both)")
print()

# 3.4 Boolean Variables
a = True
if a:
    print("a is True")
if not a:
    print("a is False")

b = False
if a and b:
    print("a and b are True")

a = 3
b = 3
# c is True if a and b are equal
c = a == b
print(c)  # prints "True"

if 1:
    print("1")  # prints
if "A":
    print("A")  # prints
if 0:
    print("Zero")  # doesn't print because 0 == False
print()

# 3.5 Else and Else If
temp = int(input("What is the temperature?"))
if temp > 30:
    print("It is hot")
elif temp < 10:
    print("It is cold")
else:
    print("It is not hot")
print("Done")

# 3.6 Text Comparisons
userName = input("What is your name?")
if userName == "Ryan":
    print("Good name!")
else:
    print("Your name is okay.")
# good practice to compare in lower case
if userName.lower() == "ryan":
    print("I like your name.")
print()
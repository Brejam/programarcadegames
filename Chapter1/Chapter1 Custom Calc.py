__author__ = 'Ryan'

# 1.2.1 Printing Text
print("Hello World.")
print()

# 1.2.2 Printing Results of Expressions
print(2 + 3)
print()

# 1.2.3 Printing Multiple Items
print("Your new score is", 1030 + 10)
print()

# 1.3 Escape Codes
print("I want to print a double quote \" for some reason")
print("The file is stored in C:\\new folder")
print("This\nis\nmy\nsample.")
print()

# 1.4 Comments
# This is a comment, it begins with a # sign
# and the computer will ignore it.
'''
This is
a
multi
line
comment. Nothing
Will run in between these quotes.
print("There")
'''  # docstring

# 1.5 Assignment Operators
x = 10
print(x)
print("x")
print("x =", x)
print()

# 1.6 Variables
x = 6
X = 5
print(x)  # prints 6
print()

# 1.7 Operators
y = 1
# These do not work
# x = 5y
# x = 5(3/2)

# These do work
x = 5 * y
x = 5 * (3 / 2)

# 1.7.1 Operator Spacing
x=5*(3/2)
x = 5 * (3 / 2)
x =5 *( 3/ 2)

# 1.8 Order of Operations
average = 90 + 86 + 71 + 100 + 98 / 5  # wrong calc
average = (90 + 86 + 71 + 100 + 98) / 5
print()

# 1.9 Trig Functions
# Import the math library
# This line is done only once, and at the very top
# of the program.
from math import *
# Calculate x using sine and cosine
x = sin(0) + cos(0)

# 1.10 Custom Equation Calculators
m = 294
g = 10.5
m2 = m / g  # This uses variables instead
print(m2)

miles_driven = 294
gallons_used = 10.5
mpg = miles_driven / gallons_used
print(mpg)

miles_driven = input("Enter miles driven:")  # returns String
miles_driven = float(miles_driven)  # cast to float
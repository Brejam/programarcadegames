__author__ = 'Ryan'
# Select program to run


def temp_converter():
    temp_f = float(input("Enter temperature in Fahrenheit:"))
    # [°C] = ([°F] − 32) × 5⁄9
    temp_c = (temp_f - 32) * (5 / 9)
    print("The temperature in Celsius is", temp_c)


def trap_calc():
    print("Area of a trapezoid")
    trap_height = float(input("Enter the row of the trapezoid:"))
    trap_btm_length = float(input("Enter the length of the bottom:"))
    trap_top_length = float(input("Enter the length of the top:"))
    trap_area = ((trap_btm_length + trap_top_length) * trap_height) / 2
    print("The area is:", trap_area)


def unknown():
    print("C")


while True:
    print("'A': Temp converter")
    print("'B': Trapezoid area calculator")
    print("'C': ...")
    print()
    userInput = input("Select program to run:")

    if userInput == "A":
        temp_converter()
    elif userInput == "B":
        trap_calc()
    elif userInput == "C":
        unknown()
    else:
        break
    print()
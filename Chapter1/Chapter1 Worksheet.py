__author__ = 'Ryan'
# 1
print("Ryan")
print()

# 2
# This is a comment

# 3
print(2 / 3)  # 0.666
print(2 // 3)  # 0
print()

# 4
pi = 3.14

# 5
# 'A' has not been defined

# 6
# area_of_rectangle

# 7
# 1Apple, account number, account.number, account#, great.big.variable, 2x, total%, #left

# 8
# defined var after use

# 9
# pi = float(3.14)
# 3.14 is already a float

# 10
radius = float(input("Radius:"))
x = 3.14
pi = x
area = pi * radius ** 2
print(area)
# x is unnecessary. could use real pi
print()

# 11
x = 4
y = 5
a = ((x)*(y))
print(a)
# unnecessary brackets
print()

# 12
# x = 4
# y = 5
# a = 3(x + y)
# print(a)
# missing operator

# 13
# radius = input(float("Enter the radius:"))
# order of functions incorrect

# 14
print(2/3+4)
print(2 / 3 + 4)  # correct styling
print( 2 / 3+ 4 )
print()

# 15
# A constant is a variable that doesn't change

# 16
# Constants are named in all caps

# 17
# a double quote ", and single quote '

# 18
print("This is a double quote \"\nThis is a new line")
print()

# 19
# yes

# 20
print(3 + 4 + 5 / 3)
# brackets are in wrong place to calculate average
print()

# 21
# operators are + - / * // ** % =

# 22
x = 3
x + 1
print(x)
# prints 3
print()

# 23
# user_name = input("Enter your name: )"
# user_name = input("Enter your name:")

# 24
# value = int(input(print("Enter your age")))
value = int(input("Enter your age"))

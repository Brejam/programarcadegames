__author__ = 'Ryan'

print("This program calculates mpg.")

# Get milesDriven from user
milesDriven = input("Enter miles driven:")
# Convert text to float
milesDriven = float(milesDriven)

# Get gallons used from user
gallonsUsed = input("Enter gallons used:")
# Convert text to float
gallonsUsed = float(gallonsUsed)

# Calculate and print mpg
mpg = milesDriven / gallonsUsed
print("Miles per gallon:", mpg)
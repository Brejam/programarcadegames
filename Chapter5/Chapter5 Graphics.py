__author__ = 'Ryan'
# Import a library of functions called 'pygame'
import pygame

# Initialize the game engine
pygame.init()


def print_code():
    print('''# Import a library of functions called 'pygame'
import pygame

# Initialize the game engine
pygame.init()

# Define some colors
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

# can use hex 0x
WHITE = (0xFF, 0xFF, 0xFF)

# def pi, could also be imported from math.pi
PI = 3.141592653

# Opening and setting window size
size = (700, 500)
screen = pygame.display.set_mode(size)

# Set window title
pygame.display.set_caption("Cool Game")

# Setting up the main program loop
# loop until the user clicks the close button
done = False

# used to manage how fast the screen updates
clock = pygame.time.Clock()

# ----- Main Loop -----
while not done:
    # --- main even loop
    for event in pygame.event.get():  # user did something
        if event.type == pygame.QUIT:  # if user clicked close
            done = True  # flag to exit loop

    # --- game logic would go here

    # --- drawing code would go here
    # first, clear the screen to white. Don't put other drawing code
    # above this, or they will be erased with this command
    screen.fill(WHITE)

    # --- update screen with what we have drawn
    pygame.display.flip()

    # --- limit to 60 fps
    clock.tick(60)''')
print_code()

# Define some colors
BLACK = (0, 0, 0)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

# can use hex 0x
WHITE = (0xFF, 0xFF, 0xFF)

# def pi, could also be imported from math.pi
PI = 3.141592653

# Opening and setting window size
size = (700, 500)
screen = pygame.display.set_mode(size)

# Set window title
pygame.display.set_caption("Cool Game")

# Setting up the main program loop
# loop until the user clicks the close button
done = False

# used to manage how fast the screen updates
clock = pygame.time.Clock()

# ----- Main Loop -----
while not done:
    # --- main even loop
    for event in pygame.event.get():  # user did something
        if event.type == pygame.QUIT:  # if user clicked close
            done = True  # flag to exit loop

    # --- game logic goes here

    # --- drawing code goes here
    # first, clear the screen to white. Don't put other drawing code
    # above this, or they will be erased with this command
    screen.fill(WHITE)

    # --- update screen with what we have drawn
    pygame.display.flip()

    # --- limit to 60 fps
    clock.tick(60)

# good practice?
pygame.quit()
__author__ = 'Ryan'
# import

# initialise pygame

# Define some colours

# Set the col and row of the screen [col, row]

# title window

# Loop until the user clicks the close button

# Used to manage how fast the screen updates

# ----- Main Program Loop -----

    # --- Main event loop
    # user did something
    # if user clicked close
    # Flag to exit loop

    # --- Game logic goes here

    # --- Drawing code goes here
    # clear screen first

    # --- Update display

    # --- Limit to 60fps

# Close the window you quit
# Or can cause 'hanging'

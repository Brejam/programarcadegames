__author__ = 'Ryan'
'''
 Show how to use a sprite backed by a graphic.

 Sample Python/Pygame Programs
 Simpson College Computer Science
 http://programarcadegames.com/
 http://simpson.edu/computer-science/

 Explanation video: http://youtu.be/vRB_983kUMc
'''
# import
import pygame

PI = 3.14
# Define some colours
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (0, 0, 255)

pygame.init()

# Set the col and row of the screen [col, row]
size = (700, 500)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# ----- Main Program Loop -----
while not done:
    # --- Main event loop
    for event in pygame.event.get():  # user did something
        if event.type == pygame.QUIT:  # if user clicked close
            done = True  # Flag to exit loop

    # --- Game logic goes here

    # --- Drawing code goes here
    # clear screen first
    screen.fill(WHITE)

    # draw line (surface, color, [start pos], [end pos], col)
    pygame.draw.line(screen, GREEN, [350, 50], [350, 450], 5)

    for i in range(100):
        pygame.draw.line(screen, BLUE, [20 + i * 1.5, 300 + i * 1.5], [20, 450])

    # Draw on the screen several lines from (0,10) to (100,110)
    # 5 pixels wide using a for loop
    for y_offset in range(0, 101, 10):
        pygame.draw.line(screen, RED, [0, 10+y_offset], [100, 110+y_offset], 5)

    # Drawing a series of x's
    for x_offset in range(30, 300, 30):
        pygame.draw.line(screen, BLACK, [x_offset, 100], [x_offset-10, 90], 2)
        pygame.draw.line(screen, BLACK, [x_offset, 90], [x_offset-10, 100], 2)

    # draw rectangle (surface, colour, [xPos, yPos, xLength, yLength], col)
    # col defaults to 0 (filled) if omitted
    pygame.draw.rect(screen, RED, [101, 110, 50, 100], 3)

    # ellipse
    pygame.draw.ellipse(screen, BLACK, [101, 110, 50, 100], 3)

    # arcs
    # Draw an arc as part of an ellipse. Use radians to determine angle to draw
    # 0 rads = 3oclock. PI/2 rads = 12oclock. PI rads = 9oclock, 3PI/2 = 6oclock, 2PI = back to 3oclock
    pygame.draw.arc(screen, GREEN, [100, 100, 250, 200], PI/2, PI, 2)
    pygame.draw.arc(screen, BLACK, [100, 100, 250, 200], 0, PI/2, 2)
    pygame.draw.arc(screen, RED, [100, 100, 250, 200], 3*PI/2, 2 * PI, 2)
    pygame.draw.arc(screen, BLUE, [100, 100, 250, 200], PI, 3*PI/2, 2)

    # This draws a triangle using the polygon command
    pygame.draw.polygon(screen, BLACK, [[100,100], [0,200], [200,200]], 5)

    # Select the font to use, size, bold, italics
    font = pygame.font.SysFont('Calibri', 25, True, False)
    # Render the text. "True" means anti-aliased text.
    # Black is the color. The variable BLACK was defined
    # above as a list of [0, 0, 0]
    # Note: This line creates an image of the letters,
    # but does not put it on the screen yet.
    text = font.render("My text", True, BLACK)
    # Put the image of the text on the screen at 250x250
    screen.blit(text, [250, 250])

    # --- Update display
    pygame.display.flip()

    # --- Limit to 60fps
    clock.tick(60)

# Close the window you quit
# Or can cause 'hanging'
pygame.quit()
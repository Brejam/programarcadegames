__author__ = 'Ryan'

# 6.1 Print Statement End Characters
print("6.1 Print Statement End Characters")

print('''
print("Pink")
print("Octopus")

...which will print out:
Pink
Octopus

Using the end character. For example:
print("Pink", end="")
print("Octopus")

This will print:
PinkOctopus

We can change the end value (must be string or none):
print("Pink", end=" ")
print("Octopus")

This will print:
Pink Octopus

Here's another example, using a variable:
i = 0
print(i, end=" ")
i = 1
print(i, end=" ")

This will print:
0 1''')

# 6.2 Advanced Looping Problems
print("6.2 Advanced Looping Problems")

print('''
1. Write code that will print ten asterisks (*) like the following:
* * * * * * * * * *

for i in range(10):
    print("*", end=" ")
''')

for i in range(10):
    print("*", end=" ")

print()
print('''
2. Write code that will print the following:
* * * * * * * * * *
* * * * *
* * * * * * * * * * * * * * * * * * * *

nums = (10, 5, 20)
for num in nums:
    for i in range(num):
        print("*", end=" ")
    print()
''')
nums = (10, 5, 20)
for num in nums:
    for i in range(num):
        print("*", end=" ")
    print()

print()
print('''
3. Use two for loops, one of them nested inside the other, to print the following 10x5 rectangle:
* * * * * * * * * *
* * * * * * * * * *
* * * * * * * * * *
* * * * * * * * * *
* * * * * * * * * *

for i in range(5):
    for j in range(10):
        print("*", end=" ")
    print()
''')

for i in range(5):
    for j in range(10):
        print("*", end=" ")
    print()

print()
print('''
4. Use two for loops, one of them nested, to print the following 5x2 rectangle:
* * * * *
* * * * *

for row in range(2):
    for col in range(5):
        print("*", end=" ")
    print()
''')

for row in range(2):
    for col in range(5):
        print("*", end=" ")
    print()

print()
print('''
5. Use two for loops, one of them nested, to print the following 20x5 rectangle:
* * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * * * *

for row in range(5):
    for col in range(20):
        print("*", end=" ")
    print()
''')

for row in range(5):
    for col in range(20):
        print("*", end=" ")
    print()

print()
print('''
6. Write code that will print the following:
0 1 2 3 4 5 6 7 8 9
0 1 2 3 4 5 6 7 8 9
0 1 2 3 4 5 6 7 8 9
0 1 2 3 4 5 6 7 8 9
0 1 2 3 4 5 6 7 8 9
0 1 2 3 4 5 6 7 8 9
0 1 2 3 4 5 6 7 8 9
0 1 2 3 4 5 6 7 8 9
0 1 2 3 4 5 6 7 8 9
0 1 2 3 4 5 6 7 8 9

for row in range(10):
    for col in range(10):
        print(col, end=" ")
    print()
''')

for row in range(10):
    for col in range(10):
        print(col, end=" ")
    print()

print()
print('''
7. Adjust the prior program to print:
0 0 0 0 0 0 0 0 0 0
1 1 1 1 1 1 1 1 1 1
2 2 2 2 2 2 2 2 2 2
3 3 3 3 3 3 3 3 3 3
4 4 4 4 4 4 4 4 4 4
5 5 5 5 5 5 5 5 5 5
6 6 6 6 6 6 6 6 6 6
7 7 7 7 7 7 7 7 7 7
8 8 8 8 8 8 8 8 8 8
9 9 9 9 9 9 9 9 9 9

for row in range(10):
    for col in range(10):
        print(row, end=" ")
    print()
''')

for row in range(10):
    for col in range(10):
        print(row, end=" ")
    print()

print()
print('''
8. Write code that will print the following:

0
0 1
0 1 2
0 1 2 3
0 1 2 3 4
0 1 2 3 4 5
0 1 2 3 4 5 6
0 1 2 3 4 5 6 7
0 1 2 3 4 5 6 7 8
0 1 2 3 4 5 6 7 8 9

i = 1
for row in range(10):
    for col in range(i):
        print(col, end=" ")
    i += 1
    print()
''')
i = 1
for row in range(10):
    for col in range(i):
        print(col, end=" ")
    i += 1
    print()

print()
print('''
9. Write code that will print the following:

0 1 2 3 4 5 6 7 8 9
  0 1 2 3 4 5 6 7 8
    0 1 2 3 4 5 6 7
      0 1 2 3 4 5 6
        0 1 2 3 4 5
          0 1 2 3 4
            0 1 2 3
              0 1 2
                0 1
                  0

i = 10
for row in range(10):
    for space in range(10 - i):
        print(" ", end=" ")
    for num in range(i):
        print(num, end=" ")
    i -= 1
    print()
''')

i = 10
for row in range(10):
    for space in range(10 - i):
        print(" ", end=" ")
    for num in range(i):
        print(num, end=" ")
    i -= 1
    print()

print()
print('''
10. Write code that will print the following (Getting the alignment is hard, at least get the numbers):
1  2  3  4  5  6  7  8  9
2  4  6  8 10 12 14 16 18
3  6  9 12 15 18 21 24 27
4  8 12 16 20 24 28 32 36
5 10 15 20 25 30 35 40 45
6 12 18 24 30 36 42 48 54
7 14 21 28 35 42 49 56 63
8 16 24 32 40 48 56 64 72
9 18 27 36 45 54 63 72 81

for row in range(9):
    for col in range(9):
        value = (row + 1) * (col + 1)
        if value < 10:
            print(" ", end="")
        print(value, end= " ")
    print()
''')

for row in range(9):
    for col in range(9):
        value = (row + 1) * (col + 1)
        if value < 10:
            print(" ", end="")
        print(value, end= " ")
    print()

print()
print('''
11. Write code that will print the following:

                  1
                1 2 1
              1 2 3 2 1
            1 2 3 4 3 2 1
          1 2 3 4 5 4 3 2 1
        1 2 3 4 5 6 5 4 3 2 1
      1 2 3 4 5 6 7 6 5 4 3 2 1
    1 2 3 4 5 6 7 8 7 6 5 4 3 2 1
  1 2 3 4 5 6 7 8 9 8 7 6 5 4 3 2 1

for row in range(10):
    for col in range(10-row):
        print(" ", end=" ")
    for col in range(row):
        print(col + 1, end=" ")
    for col in range(row - 1, 0, -1):
        print(col, end=" ")
    print()
''')
for row in range(1, 10):
    for col in range(10-row):
        print(" ", end=" ")
    for col in range(1, row+1):
        print(col, end=" ")
    for col in range(row - 1, 0, -1):
        print(col, end=" ")
    print()

print()
print('''
12. Write code that will print the following:

                  1
                1 2 1
              1 2 3 2 1
            1 2 3 4 3 2 1
          1 2 3 4 5 4 3 2 1
        1 2 3 4 5 6 5 4 3 2 1
      1 2 3 4 5 6 7 6 5 4 3 2 1
    1 2 3 4 5 6 7 8 7 6 5 4 3 2 1
  1 2 3 4 5 6 7 8 9 8 7 6 5 4 3 2 1
    1 2 3 4 5 6 7 8
      1 2 3 4 5 6 7
        1 2 3 4 5 6
          1 2 3 4 5
            1 2 3 4
              1 2 3
                1 2
                  1

for row in range(1, 10):
    for col in range(10 - row):
        print(" ", end=" ")
    for col in range(1, row + 1):
        print(col, end=" ")
    for col in range(row, 1, -1):
        print(col - 1, end=" ")
    print()
i = 9
for row in range(10):
    for col in range(10 - i + 1):
        print(" ", end=" ")
    for col in range(1, i):
        print(col, end=" ")
    i -= 1
    print()
''')

for row in range(1, 10):
    for col in range(10 - row):
        print(" ", end=" ")
    for col in range(1, row + 1):
        print(col, end=" ")
    for col in range(row, 1, -1):
        print(col - 1, end=" ")
    print()
i = 9
for row in range(10):
    for col in range(10 - i + 1):
        print(" ", end=" ")
    for col in range(1, i):
        print(col, end=" ")
    i -= 1
    print()

print()
print('''
13. Write code that will print the following:

                  1
                1 2 1
              1 2 3 2 1
            1 2 3 4 3 2 1
          1 2 3 4 5 4 3 2 1
        1 2 3 4 5 6 5 4 3 2 1
      1 2 3 4 5 6 7 6 5 4 3 2 1
    1 2 3 4 5 6 7 8 7 6 5 4 3 2 1
  1 2 3 4 5 6 7 8 9 8 7 6 5 4 3 2 1
    1 2 3 4 5 6 7 8 7 6 5 4 3 2 1
      1 2 3 4 5 6 7 6 5 4 3 2 1
        1 2 3 4 5 6 5 4 3 2 1
          1 2 3 4 5 4 3 2 1
            1 2 3 4 3 2 1
              1 2 3 2 1
                1 2 1
                  1

for row in range(10):
    for col in range(10 - row):
        print(" ", end=" ")
    for col in range(row):
        print(col + 1, end=" ")
    for col in range(row, 1, -1):
        print(col - 1, end=" ")
    print()

for row in range(1, 10):
    for col in range(row + 1):
        print(" ", end=" ")
    for col in range(9 - row):
        print(col + 1, end=" ")
    for col in range(8, row, -1):
        print(col - row, end=" ")
    print()
''')

for row in range(10):
    for col in range(10 - row):
        print(" ", end=" ")
    for col in range(row):
        print(col + 1, end=" ")
    for col in range(row, 1, -1):
        print(col - 1, end=" ")
    print()

for row in range(1, 10):
    for col in range(row + 1):
        print(" ", end=" ")
    for col in range(9 - row):
        print(col + 1, end=" ")
    for col in range(8, row, -1):
        print(col - row, end=" ")
    print()
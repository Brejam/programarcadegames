__author__ = 'Ryan'
print('''
6.1 Write a Python program that will print the following:

10
11 12
13 14 15
16 17 18 19
20 21 22 23 24
25 26 27 28 29 30
31 32 33 34 35 36 37
38 39 40 41 42 43 44 45
46 47 48 49 50 51 52 53 54

counter = 10
for row in range(10):
    for col in range(row):
        print(counter, end=" ")
        counter += 1
    print()
''')
counter = 10
for row in range(10):
    for col in range(row):
        print(counter, end=" ")
        counter += 1
    print()

print()
print('''
6.2 Create a big box out of n rows of little o's for any desired size n.
Use an input statement to allow the user to enter the value for n and then print the properly sized box.

E.g. n = 3
oooooo
o    o
oooooo

E.g. n = 8
oooooooooooooooo
o              o
o              o
o              o
o              o
o              o
o              o
oooooooooooooooo

num = int(input("Box size: "))
# box top
print("o" * num * 2)
# box sides
for row in range(num - 2):
    print("o", end="")
    print(" " * (num * 2 - 2), end="")
    print("o")
# box bottom
print("o" * num * 2)
''')

# num = int(input("Box size: "))
num = 2
# box top
print("o" * num * 2)
# box sides
for row in range(num - 2):
    print("o", end="")
    print(" " * (num * 2 - 2), end="")
    print("o")
# box bottom
print("o" * num * 2)

print()
print('''
6.3 Print the following for any positive integer n.
Use an input statement to allow the user to enter the value for n and then print the properly sized box.

E.g. n = 3
1 3 5 5 3 1
3 5     5 3
5         5
5         5
3 5     5 3
1 3 5 5 3 1

E.g. n = 5
1 3 5 7 9 9 7 5 3 1
3 5 7 9     9 7 5 3
5 7 9         9 7 5
7 9             9 7
9                 9
9                 9
7 9             9 7
5 7 9         9 7 5
3 5 7 9     9 7 5 3
1 3 5 7 9 9 7 5 3 1
''')
print("Really tough. To come back to.")
n = 3
m = n * 2 - 1
for row in range(2, m, 2):
    for col in range(2, m, 2):
        print((col*row) - n, end=" ")
    for col in range(m, 1, -2):
        print((col*row) - n, end=" ")
    print()

print()
print('''
6.4 Use nested for loops to draw small green rectangles.

Start with the pygame template code:
pygame_base_template.py
''')
# import
import pygame

# initialise pygame
pygame.init()

# Define some colours
GREEN = (0, 255, 0)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)

# Set the col and row of the screen [col, row]
size = (700, 500)
screen = pygame.display.set_mode(size)

# title window
pygame.display.set_caption = "Green Rectangles"

# Loop until the user clicks the close button
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# ----- Main Program Loop -----
while not done:

    # --- Main event loop
    for event in pygame.event.get():  # user did something
        if event.type == pygame.QUIT:  # if user clicked close
            done = True  # Flag to exit loop

    # --- Game logic goes here

    # --- Drawing code goes here
    # clear screen first
    screen.fill(BLACK)
    rectSize = 5

    for row in range(0, size[0], rectSize * 2):
        for col in range(0, size[1], rectSize * 2):
            pygame.draw.rect(screen, GREEN, [row, col, rectSize, rectSize])

    # --- Update display
    pygame.display.flip()

    # --- Limit to 60fps
    clock.tick(60)
# Close the window you quit
# Or can cause 'hanging'!
pygame.quit()
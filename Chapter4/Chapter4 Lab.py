__author__ = 'Ryan'
# Camel Text Game
import random

milesTraveled = 0
thirst = 0
camelTiredness = 0
nativesMilesTraveled = -20
drinksRemaining = 3

print("Welcome to Camel!")
print("You have stolen a camel to make your way across the great Mobi desert.")
print("The natives want their camel back and are chasing you down! Survive your")
print("desert trek and outrun the natives.")
print()

done = False
while not done:
    print("A. Drink from your canteen.")
    print("B. Ahead moderate speed.")
    print("C. Ahead full speed.")
    print("D. Stop for the night.")
    print("E. Status check.")
    print("Q. Quit.")
    userInput = input("Your choice? ").lower()
    print()

    if userInput == "q":
        done = True

    elif userInput == "e":
        print("Miles traveled:", milesTraveled)
        print("Drinks in canteen:", drinksRemaining)
        print("The natives are", milesTraveled - nativesMilesTraveled, "miles behind you.")

    elif userInput == "d":
        camelTiredness = 0
        print("The camel is happy.")
        nativesMilesTraveled += random.randrange(6, 14)

    elif userInput == "c":
        distanceTraveled = random.randrange(10, 21)
        milesTraveled += distanceTraveled
        print("You traveled", distanceTraveled, "miles.")
        thirst += 1
        camelTiredness += random.randrange(1, 4)
        nativesMilesTraveled += random.randrange(6, 14)

    elif userInput == "b":
        distanceTraveled = random.randrange(5, 13)
        milesTraveled += distanceTraveled
        print("You traveled", distanceTraveled, "miles.")
        thirst += 1
        camelTiredness += random.randrange(1, 3)
        nativesMilesTraveled += random.randrange(7, 15)

    elif userInput == "a":
        if drinksRemaining > 0:
            drinksRemaining -= 1
            thirst = 0
            print("You quench your thirst.")
        else:
            print("Your canteen is empty.")

    if thirst > 4:
        print("You are thirsty.")
    if thirst > 6:
        print("You died of thirst.")
        done = True

    if camelTiredness > 5:
        print("Your camel is getting tired.")
    if camelTiredness > 8:
        print("Your camel has died of exhaustion.")
        done = True

    if not done and nativesMilesTraveled >= milesTraveled:
        print("The natives have caught you.")
        done = True
    if not done and milesTraveled - nativesMilesTraveled < 15:
        print("The natives are getting close!")

    if not done and milesTraveled >= 200:
        print("You crossed the desert and escaped!")
        done = True

    if not done and not userInput == "e" and random.randrange(20) == 1:
        print("You found an oasis")
        print("You refill your canteen, quench your thirst and let the camel rest.")
        drinksRemaining = 3
        thirst = 0
        camelTiredness = 0

    print()

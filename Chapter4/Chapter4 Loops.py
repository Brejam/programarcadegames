__author__ = 'Ryan'
# for loops
print("for LOOPS!")
print()

for i in range(3):
    print("Please,")
print("Feed me!")
print()

# for loops and ranges
print("Printing ranges")
for i in range(30, 50, 3):  # start of range, end of range(not included), step
    print(i)
print()

print("Printing list of values")
for i in [7, 4, 3, 6]:
    print(i)
print()

print("Summing range")
a = 0
for i in range(5):
    a += i + 1
print(a)
print()

# Nested for Loops
print("Nested for Loops")
# What is the value of a?
a = 0
for i in range(10):
    a += 1
print(a)

# What is the value of a?
a = 0
for i in range(10):
    a += 1
for j in range(10):
    a += 1
print(a)

# What is the value of a?
a = 0
for i in range(10):
    a += 1
    for j in range(10):
        a += 1
print(a)
print()

# While Loops
print("While LOOPS!")
print()

i = 1
while i <= 2 ** 8:
    print(i)
    i *= 2
print()

# While loops can continue until a trigger e.g. user input
inBattle = True
while inBattle:
    escape = input("Run away? ").lower()
    if escape == "y":
        inBattle = False
        break
    attack = input("Attack enemy? ")
    if attack == "y":
        print("Enemy countered. You died.")
        inBattle = False
    else:
        print("Enemy struck first. You died.")
        inBattle = False
print()

value = 0
increment = 0.5
while value < 0.999:
    value += increment
    increment *= 0.5
    print(value)
print()

# Random Numbers
print("Random Numbers!")
print()
import random

randomNumber = random.randrange(50)  # random number from 0 - 49
print(randomNumber)

randomNumber = random.randrange(100, 201)  # random number from 100 - 200
print(randomNumber)
print()

# random.random() = float 0 - 1
randomNumber = random.random()
print(randomNumber)